package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class MatrixTest {
    Matrix one = new Matrix();
    Matrix two = new Matrix(2, 3);
    Matrix three = new Matrix(new double[][]{{6, 4}, {4, 3}});

    @Test
    public void testGetStrings() {
        assertEquals(one.getStrings(), 0, 0.001);
        assertEquals(two.getStrings(), 2, 0.001);
        assertEquals(three.getStrings(), 2, 0.001);
    }

    @Test
    public void testGetColumns() {
        assertEquals(one.getColumns(), 0, 0.001);
        assertEquals(two.getColumns(), 3, 0.001);
        assertEquals(three.getColumns(), 2, 0.001);
    }


    @Test
    public void testGetAt() {
        assertEquals(three.getAt(0, 0), 6, 0.001);
        assertEquals(three.getAt(1, 1), 3, 0.001);
        assertEquals(two.getAt(1, 0), 0, 0.001);
    }

    @Test
    public void testSetAt() {
        two.setAt(1, 1, 7);
        assertEquals(two.getAt(1, 1), 7, 0.001);
        two.setAt(0, 0, 11);
        assertEquals(two.getAt(0, 0), 11, 0.001);
    }

    @Test
    public void testToString() {
        Matrix oneMatrix = new Matrix(2, 2);
        oneMatrix.setAt(0, 0, -3);
        oneMatrix.setAt(0, 1, 4);
        oneMatrix.setAt(1, 0, 7);
        oneMatrix.setAt(1, 1, 9);
        System.out.println(oneMatrix + "\n");
        Matrix twoMatrix = new Matrix(3, 1);
        twoMatrix.setAt(0, 0, 11);
        twoMatrix.setAt(2, 0, 10);
        twoMatrix.setAt(0, 0, 11);
        System.out.println(twoMatrix + "\n");
    }
}