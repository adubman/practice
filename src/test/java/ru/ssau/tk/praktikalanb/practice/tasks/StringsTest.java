package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static ru.ssau.tk.praktikalanb.practice.tasks.Strings.methodReturningNumberOfRows;
import static ru.ssau.tk.praktikalanb.practice.tasks.Strings.returnIndexCharacterOfFirstLine;


public class StringsTest {
    @Test
    public void testCharacterOnSeparateLine() {
        String line = "Samara University";
        char[] symbols = Strings.characterOnSeparateLine(line);
        for (int i = 0; i < symbols.length; i++) {
            System.out.println(symbols[i]);
        }
    }


    @Test
    public void testByteArray() {
        String line = "Samara University";
        String lineOne = "Самарский Университет";
        Strings.byteArray(line);
        System.out.println("--------------");
        Strings.byteArray(lineOne);
    }

    @Test
    public void testComparisonsOfTwoLines() {
        Strings.comparisonsOfTwoLines();
    }

    @Test
    public void testStringIsPalindrome() {
        assertTrue(Strings.stringIsPalindrome("anna"));
        assertTrue(Strings.stringIsPalindrome("frrf"));
        assertFalse(Strings.stringIsPalindrome("adcg"));
        assertFalse(Strings.stringIsPalindrome("ayyy"));
    }

    @Test
    public void testStringsAreDifferent() {
        String lineOne = "summer";
        String lineTwo = "SUMMER";
        String lineThree = "winter";
        String lineFour = "WINTER";
        String lineFive = "summer";
        assertTrue(Strings.stringsAreDifferent(lineOne, lineTwo));
        assertFalse(Strings.stringsAreDifferent(lineOne, lineFive));
        assertFalse(Strings.stringsAreDifferent(null, lineOne));
        assertFalse(Strings.stringsAreDifferent(lineThree, null));
        assertFalse(Strings.stringsAreDifferent(lineOne, lineFour));
    }

    @Test
    public void testReturnIndexCharacterOfFirstLine() {
        String firstLine = "New Year is coming soon";
        String secondLine = "But right after this holiday session";
        assertEquals(returnIndexCharacterOfFirstLine(firstLine, "is"), 9);
        assertEquals(returnIndexCharacterOfFirstLine(secondLine, "sa"), -1);
    }

    @Test
    public void testIndexOfFirstOccurrenceOfSecondRowInSecondHalfOfFirstRow() {
        String oneLine = "фасад";
        String twoLine = "сад";
        assertEquals(Strings.indexOfFirstOccurrenceOfSecondRowInSecondHalfOfFirstRow(oneLine, twoLine), 2);
        String threeLine = "слабенький-слабенький";
        String fourLine = "бе";
        assertEquals(Strings.indexOfFirstOccurrenceOfSecondRowInSecondHalfOfFirstRow(threeLine, fourLine), 14);
        String fiveLine = "ый";
        assertEquals(Strings.indexOfFirstOccurrenceOfSecondRowInSecondHalfOfFirstRow(fourLine, fiveLine), -1);
    }

    @Test
    public void testIndexOfLastOccurrenceOfSecondRowInFirstHalfOfFirstRow() {
        String firstLine = "до-ми-соль-соль-ми-до";
        String secondLine = "ми";
        assertEquals(Strings.indexOfLastOccurrenceOfSecondRowInFirstHalfOfFirstRow(firstLine, secondLine), 3);
        String thirdLine = "соль-соль-соль-соль";
        String fourLine = "соль";
        String fiveLine = "до";
        assertEquals(Strings.indexOfLastOccurrenceOfSecondRowInFirstHalfOfFirstRow(thirdLine, fourLine), 5);
        assertEquals(Strings.indexOfLastOccurrenceOfSecondRowInFirstHalfOfFirstRow(thirdLine, fiveLine), -1);
    }

    @Test
    public void testReplacementInFirstLineEachOccurrenceOfSecondLineOnThirdLine() {
        assertEquals(Strings.replacementInFirstLineEachOccurrenceOfSecondLineOnThirdLine("kgfgf", "gf", "nn"), "knnnn");
        assertEquals(Strings.replacementInFirstLineEachOccurrenceOfSecondLineOnThirdLine("iiiikukklkup", "ku", "av"), "iiiiavkklavp");
    }

    @Test
    public void testMethodReturningNumberOfRows() {
        String[] lines = {"forest", "family", "sea", "table", "table"};
        assertEquals(methodReturningNumberOfRows(lines, "t", "e"), 2);
        assertEquals(methodReturningNumberOfRows(lines, "f", "t"), 1);
        assertEquals(methodReturningNumberOfRows(lines, "f", "a"), 0);
    }

    @Test
    public void testStringAndTwoIntegers() {
        assertEquals(Strings.stringAndTwoIntegers("fss", 0, 2), "fs");
        assertEquals(Strings.stringAndTwoIntegers("fss", -1, 100), "fss");
        assertEquals(Strings.stringAndTwoIntegers("fss", 3, 2), "");
    }

    @Test
    public void testNumberOfLinesFromPrefixToPostfixIgnoringSpaces() {
        String[] lineOne = new String[]{"  dayhk  ", "   daghk  ", "  abuhk", " abpde  "};
        assertEquals(Strings.numberOfLinesFromPrefixToPostfixIgnoringSpaces(lineOne, "da", "hk"), 2, 0.001);
    }

    @Test
    public void testReplaceEachEvenCharacterWithNumberOfThatCharacterAndFlipLine() {
        assertEquals(Strings.replaceEachEvenCharacterWithNumberOfThatCharacterAndFlipLine("самара"), "а4а2а0");
        assertEquals(Strings.replaceEachEvenCharacterWithNumberOfThatCharacterAndFlipLine("новый"), "4ы2о0");
    }

    @Test
    public void testTranslationOfInputStringFromFirstEncodingToSecond() {
        String stringOne = "Когда что-то кончается в жизни, будь то плохое или хорошее, остаётся пустота. Но пустота, оставшаяся после плохого, заполняется сама собой. Пустоту же после хорошего можно заполнить только отыскав что-то лучшее…";
        String array = Strings.translationOfInputStringFromFirstEncodingToSecond(stringOne, StandardCharsets.UTF_8, StandardCharsets.UTF_16BE);
        System.out.println("Строка с первой кодировкой: " + stringOne + "\n" + "Строка после замены кодировки: " + array);
        //И вот. что вывелось:
        /*Строка с первой кодировкой: Когда что-то кончается в жизни, будь то плохое или хорошее, остаётся пустота. Но пустота, оставшаяся после плохого, заполняется сама собой. Пустоту же после хорошего можно заполнить только отыскав что-то лучшее…
        Строка после замены кодировки: 킚킾킳킴킰⃑蟑苐븭톂킾⃐뫐뻐뷑蟐냐뗑苑臑輠킲⃐뛐룐럐뷐렬⃐뇑菐듑谠톂킾⃐뿐믐뻑藐뻐딠킸킻킸⃑藐뻑胐뻑裐뗐딬⃐뻑臑苐냑金苑臑輠킿톃톁톂킾톂킰⸠킝킾⃐뿑菑臑苐뻑苐뀬⃐뻑臑苐냐닑裐냑近臑輠킿킾톁킻킵⃐뿐믐뻑藐뻐돐븬⃐럐냐뿐뻐믐뷑运뗑苑臑輠톁킰킼킰⃑臐뻐뇐뻐뤮⃐鿑菑臑苐뻑苑茠킶킵⃐뿐뻑臐믐딠톅킾톀킾톈킵킳킾⃐볐뻐뛐뷐븠킷킰킿킾킻킽킸톂톌⃑苐뻐믑賐뫐븠킾톂톋톁킺킰킲⃑蟑苐븭톂킾⃐믑菑蟑裐뗐뗢肦*/
    }

    @Test
    public void testPrintInConsole() {
        Matrix matrix = new Matrix(2, 2);
        matrix.setAt(0, 1, 4);
        matrix.setAt(0, 0, 6);
        matrix.setAt(1, 1, 5);
        matrix.setAt(1, 0, 3);
        Strings.printInConsole(matrix);
        System.out.println("---------------");
        Strings.printInConsole(new Person("MALE", "Ivan", "Ivanov", 788932));
        System.out.println("---------------");
        Strings.printInConsole(new Point(34, 23, 12));
        System.out.println("---------------");
        Strings.printInConsole(new NamedPoint("x", 11, 11, 11));
        System.out.println("---------------");
        Strings.printInConsole(11);
        Strings.printInConsole(11.11);
    }

    @Test
    public void testFirstCharacterMustBeUppercase() {
        String string = " тот, кто хочет видеть результаты своего труда немедленно, должен идти в сапожники. альберт эйнштейн";
        String[] arrayString = Strings.firstCharacterMustBeUppercase(string);
        for (String a : arrayString) {
            System.out.println(a);
        }
    }

    @Test
    public void testDefaultEncodings() {
        Strings.defaultEncodings();
        //И вот. что вывелось:
        //UTF-8
    }

    @Test
    public void testCombineInOneLine() {
        String[] string = new String[]{"Секрет того", "чтобы добиться чего-то - начать."};
        assertEquals(Strings.combineInOneLine(string), "Секрет того, чтобы добиться чего-то - начать.");
        String[] secondTwo = new String[]{"Успех - это умение двигаться от неудачи к неудаче", "не теряя энтузиазма."};
        assertEquals(Strings.combineInOneLine(secondTwo), "Успех - это умение двигаться от неудачи к неудаче, не теряя энтузиазма.");
    }

    @Test
    public void testFormStringConsistingOfEnumeration() {
        assertEquals(Strings.formStringConsistingOfEnumeration(3), "0 1 2");
        String string = Strings.formStringConsistingOfEnumeration(10000);
        System.out.println(string);
    }

}