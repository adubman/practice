package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import java.util.Collection;

import static org.testng.Assert.*;

public class ArrayTest {
    @Test
    public void testGetRandomArray() {
        double[] massif = Array.getNewArray(11);
        assertEquals(massif.length, 11, 0.001);
    }

    @Test
    public void testGetOneArray() {
        int count = 11;
        double[] array = Array.getOneArray(count);
        for (int i = 0; i < count; i++) {
            if (i == 0 || i == count) {
                assertEquals(array[i], 2, 0.001);
            } else {
                assertEquals(array[i], 1, 0.001);
            }
        }
    }

    @Test
    public void testGetOddArray() {
        int count = 11;
        double[] array = Array.getOddArray(count);
        for (int i = 0; i < count; i++) {
            assertEquals(array[i], i * 2 + 1, 0.001);
        }
    }

    @Test
    public void testGetEvenArray() {
        int count = 11;
        double[] array = Array.getEvenArray(count);
        for (int i = count - 1; i >= 0; i--) {
            assertEquals(array[i], i * 2 + 2, 0.0001);
            assertEquals(array[i], i * 2 + 2, 0.0001);
        }
    }

    @Test
    public void testFibonacci() {
        int count = 5;
        double[] array = Array.getFibonacci(count);
        assertEquals(array[count - 1], 3, 0.01);
    }

    @Test
    public void testGetArrayOfSquares() {
        int count = 11;
        double[] array = Array.getArrayOfSquares(count);
        assertEquals(array[count - 1], 100, 0.01);
    }

    @Test
    public void testGetArrayNotDivisibleByThree() {
        double[] array = Array.getArrayNotDivisibleByThree(8);
        assertEquals(array[0], 1, 0.01);
        assertEquals(array[1], 2, 0.01);
        assertEquals(array[2], 4, 0.01);
        assertEquals(array[3], 5, 0.01);
        assertEquals(array[4], 7, 0.01);
        assertEquals(array[5], 8, 0.01);
        assertEquals(array[6], 10, 0.01);
    }

    @Test
    public void testGetArrayArithmeticProgression() {
        double[] array = Array.getArrayArithmeticProgression(0, 11, 11);
        assertEquals(array[2], 22, 0.01);
        assertEquals(array[3], 33, 0.01);
        assertEquals(array[4], 44, 0.01);
    }

    @Test
    public void testGetArrayGeometricProgression() {
        double[] array = Array.getArrayGeometricProgression(2, 2, 11);
        assertEquals(array[1], 4, 0.01);
        assertEquals(array[2], 8, 0.01);
        assertEquals(array[3], 16, 0.01);
    }

    @Test
    public void testGetQuadraticSolution() {
        double[] array = Array.getQuadraticSolution(2, 7, 3);
        assertEquals(array[0], -0.5, 0.01);
        assertEquals(array[1], -3, 0.01);
    }

    @Test
    public void testGetArrayIsSymmetrical() {
        int[] array = Array.getArrayIsSymmetrical(5);
        assertEquals(array[0], 1, 0.01);
        assertEquals(array[1], 2, 0.01);
        assertEquals(array[2], 3, 0.01);
        assertEquals(array[3], 2, 0.01);
        assertEquals(array[4], 1, 0.01);
    }

    @Test
    public void testGetCheckingForPresenceOfNumberInArray() {
        double[] array = new double[]{1, 2, 7};
        assertTrue(Array.getCheckingForPresenceOfNumberInArray(array, 2));
        assertFalse(Array.getCheckingForPresenceOfNumberInArray(array, 5));
    }

    @Test
    public void testGetNumberOfEvenNumbers() {
        double[] arrayOne = new double[]{11, 2, 4, 7, 8};
        assertEquals(Array.getNumberOfEvenNumbers(arrayOne), 3, 0.01);
        double[] arrayTwo = new double[]{11, 1, 4, 12, 9};
        assertEquals(Array.getNumberOfEvenNumbers(arrayTwo), 2, 0.01);
    }

    @Test
    public void testGetCheckingForNull() {
        Integer[] arrayOne = new Integer[]{11, null, 12, 3};
        assertTrue(Array.getCheckingForNull(arrayOne));
        Integer[] arrayTwo = new Integer[]{11, 2, 12, 3};
        assertFalse(Array.getCheckingForNull(arrayTwo));
    }

    @Test
    public void testSumOfAllNumbersWithEvenIndices() {
        Integer[] arrayOne = new Integer[]{3, 4, 2, 1, 9};
        assertEquals(Array.getSumOfAllNumbersWithEvenIndices(arrayOne), 14, 0.01);
        Integer[] arrayTwo = new Integer[]{11, 6, 1, 9, 7};
        assertEquals(Array.getSumOfAllNumbersWithEvenIndices(arrayTwo), 19, 0.01);
    }

    @Test
    public void testGetTakesIntegersAndReturnsBoolean() {
        Integer[] arrayOne = new Integer[]{1, 11, 3, 7, 12};
        assertTrue(Array.getTakesIntegersAndReturnsBoolean(arrayOne));
        Integer[] arrayTwo = new Integer[]{11, 8, 7, 5, 3};
        assertFalse(Array.getTakesIntegersAndReturnsBoolean(arrayTwo));
    }


    @Test
    public void testGetReverseSign() {
        int[] array = Array.getArrayIsSymmetrical(5);
        assert array != null;
        Array.getReverseSign(array);
        assertEquals(array[0], -1, 0.01);
        assertEquals(array[3], -2, 0.01);
    }

    @Test
    public void testGetTruthOrLie() {
        int[] arrayOne = new int[]{55, 1, -80, 5, 34};
        boolean[] arrayTwo = new boolean[]{false, false, true, false, true};
        assertEquals(Array.getTruthOrLie(arrayOne), arrayTwo);
    }

    @Test
    public void testGetElementThatOccursMostOftenInArray() {
        double[] arrayOne = new double[]{55, 11., -80, 5, 34, 11., 11.};
        assertEquals(Array.getElementThatOccursMostOftenInArray(arrayOne), 11, 0.01);
        double[] arrayTwo = new double[]{-55., 11.4, -80, 5, 34, -55., 11.};
        assertEquals(Array.getElementThatOccursMostOftenInArray(arrayTwo), -55, 0.01);

    }

    @Test
    public void testGetInputIsArrayOfIntegersAndOutputIsMaximumElement() {
        Integer[] arrayOne = new Integer[]{1, 5, 11, 7, 9};
        assertEquals(Array.getInputIsArrayOfIntegersAndOutputIsMaximumElement(arrayOne), 11, 0.01);
        Integer[] arrayTwo = new Integer[]{-1, 5, 6, -4, 7};
        assertEquals(Array.getInputIsArrayOfIntegersAndOutputIsMaximumElement(arrayTwo), 7, 0.01);
        Integer[] arrayThree = new Integer[]{};
        assertNull(Array.getInputIsArrayOfIntegersAndOutputIsMaximumElement(arrayThree));
    }

    @Test
    public void testIndexOIfFirstElementEqualToInputNumber() {
        double[] arrayOne = new double[]{11, 13, 11, 13, 17};
        double[] arrayTwo = new double[]{13, 11, 13, 11, 67};
        double oneNumber = 11;
        double twoNumber = 13;
        assertEquals(Array.indexOIfFirstElementEqualToInputNumber(arrayOne, oneNumber), 0);
        assertEquals(Array.indexOIfFirstElementEqualToInputNumber(arrayOne, twoNumber), 1);
        assertEquals(Array.indexOIfFirstElementEqualToInputNumber(arrayTwo, oneNumber), 1);
        assertEquals(Array.indexOIfFirstElementEqualToInputNumber(arrayTwo, twoNumber), 0);
    }

    @Test
    public void testSwapElements() {
        double[] array = new double[]{11, 4, -7, 89, -109};
        Array.swapElements(array);
        assertEquals(array[4], 89.0);
        assertEquals(array[3], -109.0);
    }

    @Test
    public void testBitNegation() {
        Integer[] arrayOne = new Integer[]{13, 9, 18};
        Array.bitNegation(arrayOne);
        assertEquals(arrayOne, new Integer[]{-14, -10, -19});
        Array.bitNegation(arrayOne);
        assertEquals(arrayOne, new Integer[]{13, 9, 18});
    }

    @Test
    public void testArrayBitNegation() {
        int[] arrayOne = new int[]{5, 6, 7};
        int[] negationArray = Array.arrayBitNegation(arrayOne);
        assertEquals(negationArray[0], -6);
        assertEquals(negationArray[1], -7);
        assertEquals(negationArray[2], -8);
        negationArray = Array.arrayBitNegation(negationArray);
        assertEquals(negationArray[0], 5);
        assertEquals(negationArray[1], 6);
        assertEquals(negationArray[2], 7);
    }

    @Test
    public void testSumOfPairwiseElements() {
        int[] arrayOne = new int[]{1, 7, 8, 3};
        int[] oneSumOfPairwiseElements = Array.sumOfPairwiseElements(arrayOne);
        assertEquals(oneSumOfPairwiseElements[0], 8);
        assertEquals(oneSumOfPairwiseElements[1], 11);
        int[] arrayTwo = new int[]{3, -3, 6};
        int[] twoSumOfPairwiseElements = Array.sumOfPairwiseElements(arrayTwo);
        assertEquals(twoSumOfPairwiseElements[0], 0);
        assertEquals(twoSumOfPairwiseElements[1], 6);
    }

    @Test
    public void testCreateTwoDimensionalArrayOfNumbers() {
        int[][] arrayOne = Array.createTwoDimensionalArrayOfNumbers(2);
        assertEquals(arrayOne[0][0], 1);
        assertEquals(arrayOne[0][1], 2);
        assertEquals(arrayOne[1][0], 3);
    }

    @Test
    public void testCreateArrayAndFillItWithNaturalNumbers() {
        int[] arrayOne = new int[]{5, 7, 5, 3};
        int index= 1;
        int[] arrayAndFillItWithNaturalNumbers = Array.createArrayAndFillItWithNaturalNumbers(arrayOne.length, index);
        assertEquals(arrayAndFillItWithNaturalNumbers[0], 4);
        assertEquals(arrayAndFillItWithNaturalNumbers[1], 1);
        assertEquals(arrayAndFillItWithNaturalNumbers[2], 2);
        assertEquals(arrayAndFillItWithNaturalNumbers[3], 3);
    }

    @Test
    public void testAllIntegerDivisors() {
        double[] arrayOne = Array.allIntegerDivisors(12);
        assertEquals(arrayOne[0],1, 0.001);
        assertEquals(arrayOne[1],2, 0.001);
        assertEquals(arrayOne[2],3, 0.001);
        assertEquals(arrayOne.length,3, 0.001);
    }

    @Test
    public void testLongToInt() {
        assertEquals(Array.intToLong(Array.longToInt(6764576756485786786L)), 6764576756485786786L);
        assertEquals(Array.intToLong(Array.longToInt(9057587654445334535L)), 9057587654445334535L);
        assertEquals(Array.intToLong(Array.longToInt(-97805364587326487L)), -97805364587326487L);
        assertEquals(Array.intToLong(Array.longToInt(-1117526543674545363L)), -1117526543674545363L);
    }

    @Test
    public static void testContainedOrNotInNanArray() {
        double[] arrayOne = new double[]{-2.2, 10.78, 9.87};
        Array.containedOrNotInNanArray(arrayOne);
        assertEquals(arrayOne[0], -2.2);
        assertEquals(arrayOne[1], 9.87);
        assertEquals(arrayOne[2], 10.78);
        double[] arrayTwo = new double[]{11.1, 12.7, Double.NaN};
        Array.containedOrNotInNanArray (arrayTwo);
        assertEquals(arrayTwo[0], 11.1);
        assertEquals(arrayTwo[1], 12.7);
        assertEquals(arrayTwo[2], Double.NaN);
    }

    @Test
    public static void testPrintAllStringValuesToConsole() {
        String[] array = new String[]{"Самарский", "национальный", "исследовательский", "университет"};
        Array. printAllStringValuesToConsole(array);
    }

    @Test
    public static void testProductOfAllNumbers() {
        double[] arrayOne = new double[]{1., 12., Double.NaN};
        assertEquals(Array.productOfAllNumbers(arrayOne), 12.0);
        double[] arrayTwo = new double[]{2., 0., 6., 4, 1.};
        assertEquals(Array.productOfAllNumbers(arrayTwo), 48.0);
        double[] arrayThree = new double[]{1., 11., 3};
        assertEquals(Array.productOfAllNumbers(arrayThree), 33.0);
        double[] arrayFour = new double[]{1., 11., Double.POSITIVE_INFINITY};
        assertEquals(Array.productOfAllNumbers(arrayFour), 11.0);
    }

    @Test
    public void testToHexString() {
        int[] array = new int[]{11, 111, 1, 12, 112};
        Collection<String> hexStringArray = Array.toHexString(array);
        for (String hexString : hexStringArray) {
            System.out.println(hexString);
        }
    }

    @Test
    public void testPositivePrimes() {
        double[] arrayOne = Array.positivePrimes(2);
        assertEquals(arrayOne[0],2, 0.01);
        assertEquals(arrayOne.length,1, 0.01);
        double[] arrayTwo = Array.positivePrimes(5);
        assertEquals(arrayTwo[0],2, 0.01);
        assertEquals(arrayTwo[1],3, 0.01);
        assertEquals(arrayTwo[2],5, 0.01);
        assertEquals(arrayTwo.length,3, 0.01);
    }
}



