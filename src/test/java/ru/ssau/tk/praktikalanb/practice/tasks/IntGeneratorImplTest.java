package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class IntGeneratorImplTest {

    @Test
    public void testNextInt() {
        IntGenerator one = new IntGeneratorImpl();
        assertEquals(one.nextInt(), 0, 0.01);
        assertEquals(one.nextInt(), 1, 0.01);
        assertEquals(one.nextInt(), 2, 0.01);
        assertEquals(one.nextInt(), 3, 0.01);
    }
}