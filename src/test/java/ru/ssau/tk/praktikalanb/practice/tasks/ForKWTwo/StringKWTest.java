package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class StringKWTest {
    @Test
    public void testFirstContainIndex() {
        assertEquals(StringKW.secondLineEntry("uiretrruik", "ui"), 7, 0.01);
        assertThrows(UndeclaredException.class, () -> StringKW.secondLineEntry("uiretrruk", "ui"));
    }
}