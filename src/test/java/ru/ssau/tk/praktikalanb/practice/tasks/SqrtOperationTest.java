package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SqrtOperationTest {
    private Operation sqrt = new SqrtOperation();

    @Test
    public void testApply() {
        assertEquals(sqrt.apply(11), 3.317, 0.001);
        assertEquals(sqrt.apply(-11), Double.NaN, 0.001);
        assertEquals(sqrt.apply(Double.NEGATIVE_INFINITY), Double.NaN, 0.001);
        assertEquals(sqrt.apply(Double.POSITIVE_INFINITY), Double.POSITIVE_INFINITY, 0.001);
    }

    @Test
    public void testApplyTriple() {
        assertEquals(sqrt.applyTriple(11), Math.pow(11, 1./8), 0.001);
        assertEquals(sqrt.applyTriple(Double.NaN), Double.NaN, 0.001);
        assertEquals(sqrt.applyTriple(Double.NEGATIVE_INFINITY), Double.NaN, 0.001);
        assertEquals(sqrt.applyTriple(Double.POSITIVE_INFINITY), Double.POSITIVE_INFINITY, 0.001);
    }
}