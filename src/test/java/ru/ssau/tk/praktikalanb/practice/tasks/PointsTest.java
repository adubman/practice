package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PointsTest {
    private Point onePoint = new Point(1, 4, 7);
    private Point twoPoint = new Point(1, 5, 7);

    @Test
    public void testSum() {
        assertTrue(Points.equalsApproximately(Points.sum(onePoint, twoPoint), new Point(2, 9, 14)));
        assertFalse(Points.equalsApproximately(Points.sum(onePoint, twoPoint), new Point(2, 11, 14)));
    }

    @Test
    public void testSubtract() {
        assertTrue(Points.equalsApproximately(Points.subtract(onePoint, twoPoint), new Point(0, -1, 0)));
        assertFalse(Points.equalsApproximately(Points.subtract(onePoint, twoPoint), new Point(0, -1, 11)));
    }

    @Test
    public void testMultiply() {
        assertTrue(Points.equalsApproximately(Points.multiply(onePoint, twoPoint), new Point(1, 20, 49)));
        assertFalse(Points.equalsApproximately(Points.multiply(onePoint, twoPoint), new Point(0, -1, 0)));
    }

    @Test
    public void testDivide() {
        assertTrue(Points.equalsApproximately(Points.divide(onePoint, twoPoint), new Point(1, 0.8, 1)));
        assertFalse(Points.equalsApproximately(Points.divide(onePoint, twoPoint), new Point(1, 8, 1)));
    }

    @Test
    public void testEnlarge() {
        assertTrue(Points.equalsApproximately(Points.enlarge(onePoint, 4), new Point(4, 16, 28)));
        assertFalse(Points.equalsApproximately(Points.enlarge(onePoint, 4), new Point(4, 16, 70)));
    }

    @Test
    public void testOpposite() {
        assertTrue(Points.equalsApproximately(Points.opposite(onePoint), new Point(-1, -4, -7)));
        assertFalse(Points.equalsApproximately(Points.opposite(onePoint), new Point(-1, -9, -7)));
    }

    @Test
    public void testInverse() {
        assertTrue(Points.equalsApproximately(Points.inverse(new Point(5, 2, 4)), new Point(0.2, 0.5, 0.25)));
        assertFalse(Points.equalsApproximately(Points.inverse(new Point(5, 2, 4)), new Point(0.2, 0.5, 0.78)));
    }

    @Test
    public void testScalarProduct() {
        assertEquals(Points.scalarProduct(onePoint, twoPoint), 70.0);
    }

    @Test
    public void testVectorProduct() {
        assertTrue(Points.equalsApproximately(Points.vectorProduct(new Point(3.0, 2.0, 1.0), new Point(1.0, 2.0, 3.0)), new Point(4.0, -8.0, 4.0)));
        assertFalse(Points.equalsApproximately(Points.vectorProduct(new Point(3.0, 2.0, 1.0), new Point(1.0, 2.0, 3.0)), new Point(4.9, -8.0, 4.6)));
    }



    @Test
    public void testLength() {
        assertEquals(Points.length(onePoint),Math.sqrt(1*1+4*4+7*7),0.000001);
    }

}