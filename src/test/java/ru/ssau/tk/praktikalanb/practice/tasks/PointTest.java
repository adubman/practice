package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PointTest {

    @Test
    public void testToString() {
        assertEquals(new Point(3,7,11).toString(), "[3.0, 7.0, 11.0]");
        assertEquals(new Point(-8,90,6).toString(), "[-8.0, 90.0, 6.0]");
    }
}