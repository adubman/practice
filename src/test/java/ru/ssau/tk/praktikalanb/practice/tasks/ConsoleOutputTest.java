package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ConsoleOutputTest {
        @Test
        public void printType() {
            ConsoleOutput.printType((byte) 6);
            ConsoleOutput.printType('m');
            ConsoleOutput.printType((short) 6);
            ConsoleOutput.printType(6);
            ConsoleOutput.printType(6L);
            ConsoleOutput.printType(6.1f);
            ConsoleOutput.printType(5.6d);
            ConsoleOutput.printType(false);
            ConsoleOutput.printType(new Person());
            ConsoleOutput.printType(new Point(1, 1, 2));
            ConsoleOutput.printType(Integer.valueOf(6));
            ConsoleOutput.printType("h");
        }
}