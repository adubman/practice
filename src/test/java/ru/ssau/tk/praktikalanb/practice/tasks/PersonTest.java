package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;
//import ru.ssau.tk.praktikalanb.practice.tasks.Person;

import static org.testng.Assert.*;

public class PersonTest {
    private Person firstPerson = new Person();
    private Person secondPerson = new Person(null, null);
    private Person thirdPerson = new Person("FEMALE","Lyuba", "Ivanova", 234567);
    private Person fourthPerson = new Person();

    @Test
    public void testFirstName() {
        firstPerson.setFirstName("Anna");
        assertEquals(firstPerson.getFirstName(), "Anna");
        secondPerson.setFirstName(null);
        assertNull(secondPerson.getFirstName());
        assertEquals(thirdPerson.getFirstName(), "Lyuba");
        assertNull(fourthPerson.getFirstName());
    }

    @Test
    public void testLastName() {
        firstPerson.setLastName(null);
        assertNull(firstPerson.getLastName());
        secondPerson.setLastName("Koplova");
        assertEquals(secondPerson.getLastName(), "Koplova");
        assertEquals(thirdPerson.getLastName(), "Ivanova");
        assertNull(fourthPerson.getLastName());
    }

    @Test
    public void testGetPassportId() {
        assertEquals(firstPerson.getPassportId(), 0);
        secondPerson.setPassportId(344578);
        assertEquals(secondPerson.getPassportId(), 344578);
        thirdPerson.setPassportId(764523);
        assertEquals(thirdPerson.getPassportId(), 764523);
        assertEquals(fourthPerson.getPassportId(), 0);
    }

    @Test
    public void testGetGender() {
        firstPerson.setGender(Gender.FEMALE);
        assertEquals(firstPerson.getGender(), Gender.FEMALE);
        secondPerson.setGender(Gender.MALE);
        assertEquals(secondPerson.getGender(), Gender.MALE);

    }

    @Test
    public void testToString() {
        Person PersonFirst = new Person("MALE","Kevin", "Forst", 235672);
        Person PersonSecond = new Person("FEMALE","Roza", "Forst", 345626);
        assertEquals(PersonFirst.toString(), "Kevin Forst");
        assertEquals(PersonSecond.toString(), "Roza Forst");
    }


}