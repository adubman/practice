package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class IdenticalOperationTest {
    private Operation num = new IdenticalOperation();

    @Test
    public void testApply() {
        assertEquals(num.apply(11), 11, 0.001);
        assertEquals(num.apply(Double.NaN), Double.NaN, 0.001);
        assertEquals(num.apply(Double.NEGATIVE_INFINITY), Double.NEGATIVE_INFINITY, 0.001);
        assertEquals(num.apply(Double.POSITIVE_INFINITY), Double.POSITIVE_INFINITY, 0.001);
    }

    @Test
    public void testApplyTriple() {
        assertEquals(num.applyTriple(11), 11, 0.001);
        assertEquals(num.applyTriple(Double.NaN), Double.NaN, 0.001);
        assertEquals(num.applyTriple(Double.NEGATIVE_INFINITY), Double.NEGATIVE_INFINITY, 0.001);
        assertEquals(num.applyTriple(Double.POSITIVE_INFINITY), Double.POSITIVE_INFINITY, 0.001);
    }
}