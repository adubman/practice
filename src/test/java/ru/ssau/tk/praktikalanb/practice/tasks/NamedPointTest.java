package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class NamedPointTest {
    @Test
    public void PointConstructorsOne() {
        NamedPoint namedPoint = new NamedPoint(11, 11, 11, "PointX");
        assertEquals(namedPoint.length(), 19.05, 0.01);
        assertEquals(namedPoint.getName(), "PointX");
    }

    @Test
    public void PointConstructorsTwo() {
        NamedPoint namedPoint = new NamedPoint(12, 12, 12);
        assertEquals(namedPoint.length(), 20.78, 0.01);
    }

    @Test
    public void PointConstructorsThree() {
        NamedPoint namedPoint = new NamedPoint();
        assertEquals(namedPoint.length(), 0, 0.01);
        namedPoint.setName("Origin");
        assertEquals(namedPoint.getName(), "Origin");
        namedPoint.reset();
        assertEquals(namedPoint.getName(), "Absent");
    }

    @Test
    public void testToString() {
        assertEquals(new NamedPoint( "x",2, 6, 7).toString(), "x [ 2.0, 6.0, 7.0 ]");
        assertEquals(new NamedPoint(-8, 12, 5).toString(), "[ -8.0, 12.0, 5.0 ]");
    }

}