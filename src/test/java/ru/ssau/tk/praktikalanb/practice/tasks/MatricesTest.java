package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MatricesTest {
    Matrix one = new Matrix(new double[][]{{1, 9}, {7, 4}});
    Matrix two = new Matrix(new double[][]{{1, 1, 3}, {2, 2, 8}, {2, 1, 0}});
    Matrix three = new Matrix(new double[][]{{7, 2}, {7, 3}});

    @Test
    public void testSum() {
        assertNull(Matrices.sum(one, two));
        Matrix result = Matrices.sum(one, three);
        assertEquals(result.getAt(0, 0), 8, 0.001);
        assertEquals(result.getAt(1, 1), 7, 0.001);
        assertEquals(one.getAt(0, 0), 1, 0.001);
    }

    @Test
    public void testMultiplication() {
        assertNull(Matrices.multiplication(one, two));
        Matrix result = Matrices.multiplication(one, three);
        assertEquals(result.getAt(0, 0), 70, 0.001);
        assertEquals(result.getAt(1, 1), 26, 0.001);
        assertEquals(one.getAt(0, 0), 1, 0.001);
    }

    @Test
    public void testMultiplicationAtNumber() {
        Matrix result = Matrices.multiplicationAtNumber(one, 11);
        assertEquals(result.getAt(0, 0), 11, 0.001);
        assertEquals(result.getAt(1, 1), 44, 0.001);
    }
}