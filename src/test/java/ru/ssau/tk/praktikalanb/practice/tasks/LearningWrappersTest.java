package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

public class LearningWrappersTest {
    @Test
    @Deprecated
    public void test() {
        int iOne = 11;
        int iTwo = 11;
        int iThree = 18;
        Integer I = LearningWrappers.boxing(iOne);
        Integer IFour=18;
        Integer IFive=18;
        Integer IOne=13;
        int iFirst = LearningWrappers.unboxing(IOne);
        System.out.println(iOne + " " + I);
        System.out.println(iFirst + " " + IOne);
        System.out.println("iOne == iTwo"+" "+ (iOne == iTwo));
        System.out.println("iOne == iThree"+" "+ (iOne == iThree));
        System.out.println("iThree == IFour"+" "+ (iThree == IFour));
        System.out.println("IFive == IFour"+" "+ (IFive.equals(IFour)));
        System.out.println("---------------");

        boolean b = false;
        Boolean B= LearningWrappers.boxing(b);
        Boolean BOne= true;
        boolean bOne= LearningWrappers.unboxing( BOne);
        System.out.println(b + " " + B);
        System.out.println(bOne + " " +  BOne);
        System.out.println("---------------");

        short s= 31112;
        Short S= LearningWrappers.boxing(s);
        Short SOne= 28907;
        short sOne= LearningWrappers.unboxing(SOne);
        System.out.println(s + " " + S);
        System.out.println(sOne + " " + SOne);
        System.out.println("s == i"+" "+ (s == iOne));
        System.out.println("I1 == S1"+" "+ (IOne.equals(SOne)));
        /*и так далее...*/

    }

}