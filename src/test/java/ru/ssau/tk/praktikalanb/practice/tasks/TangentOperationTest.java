package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class TangentOperationTest {
    private Operation tan = new TangentOperation();

    @Test
    public void testApply() {
        assertEquals(tan.apply(11), Math.tan(11), 0.001);
        assertEquals(tan.apply(Double.NaN), Double.NaN, 0.001);
        assertEquals(tan.apply(Double.NEGATIVE_INFINITY), Double.NaN, 0.001);
        assertEquals(tan.apply(Double.POSITIVE_INFINITY), Double.NaN, 0.001);
    }

    @Test
    public void testApplyTriple() {
        assertEquals(tan.applyTriple(11), Math.tan(Math.tan(Math.tan(11))), 0.001);
        assertEquals(tan.applyTriple(Double.NaN), Double.NaN, 0.001);
        assertEquals(tan.applyTriple(Double.POSITIVE_INFINITY), Double.NaN,0.001);
        assertEquals(tan.applyTriple(Double.NEGATIVE_INFINITY), Double.NaN, 0.001);
    }
}