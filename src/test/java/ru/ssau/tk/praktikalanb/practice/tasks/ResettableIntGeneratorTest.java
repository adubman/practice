package ru.ssau.tk.praktikalanb.practice.tasks;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ResettableIntGeneratorTest {

    @Test
    public void testNextInt() {
        IntGenerator one = new IntGeneratorImpl();
        assertEquals(one.nextInt(), 0, 0.01);
        assertEquals(one.nextInt(), 1, 0.01);
        assertEquals(one.nextInt(), 2, 0.01);
    }

    @Test
    public void testRest() {
        ResettableIntGenerator two = new ResettableIntGenerator();
        assertEquals(two.nextInt(), 0, 0.01);
        assertEquals(two.nextInt(), 1, 0.01);
        assertEquals(two.nextInt(), 2, 0.01);
        assertEquals(two.nextInt(), 3, 0.01);
        two.reset();
        assertEquals(two.nextInt(), 0, 0.01);
        assertEquals(two.nextInt(), 1, 0.01);
        two.reset();
        assertEquals(two.nextInt(), 0, 0.01);
    }
}