package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 2519156779960433677L;
    private static String result;
    private  String FName;
    private  String Name;
    private  String Patronymic;

    public Person(String FName, String Name, String Patronymic) {
        this.FName = FName;
        this.Name = Name;
        this.Patronymic = Patronymic;
    }

    String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.FName = FName;
    }

    String getPatronymic() {

        return Patronymic;
    }

    public void setPatronymic(String patronymic) {

        this.Patronymic = patronymic;
    }

   String FullNameWithSpace() {
       return FName.concat(" ").concat(Name).concat(" ").concat(Patronymic);
    }
    String FnameAndInitials() {
        return FName.concat(" ").concat(String.valueOf(Name.charAt(0))).concat(".").concat(String.valueOf(Patronymic.charAt(0))).concat(".");
    }
}

