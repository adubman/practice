package ru.ssau.tk.praktikalanb.practice.tasks;

class Main {

    public static void main(String[] args) {

        Person firstPerson = new Person("FEMALE","Lyuba", "Ivanova", 9870);
        String firstName1 = firstPerson.getFirstName();
        String lastName1 = firstPerson.getLastName();
        int passportId1 = firstPerson.getPassportId();

        Person secondPerson = new Person("FEMALE","Kate", "Ivanova", 2387);
        String firstName2 = secondPerson.getFirstName();
        String lastName2 = secondPerson.getLastName();
        int passportId2 = secondPerson.getPassportId();

        System.out.println("Имя: " + firstName1);
        System.out.println("Фамилия: " + lastName1);
        System.out.println("Паспорт: " + passportId1);

        System.out.println("Имя: " + firstName2);
        System.out.println("Фамилия: " + lastName2);
        System.out.println("Паспорт: " + passportId2);

        firstPerson.setFirstName("Anna");
        System.out.println("Изначальное имя — " + firstPerson.getFirstName());


    }
}
