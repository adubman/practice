package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.*;


//c.	Создать класс с main-методом, который будет сохранять короткое
// имя из объекта Person в текстовый символьный файл.
public class NameInCharacterFile {
    public static void main(String[] args) {
        Person person = new Person("Ivanov", "Ivan","Ivanovich");
        try(PrintWriter stream= new PrintWriter(new FileWriter("second.txt"))){
            stream.printf(person.FnameAndInitials());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

