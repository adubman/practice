package ru.ssau.tk.praktikalanb.practice.tasks;

public enum Gender {
    MALE, FEMALE
}
