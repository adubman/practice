package ru.ssau.tk.praktikalanb.practice.tasks;


class Matrix {
    private double[][] matrix;
    private final int strings;
    private final int columns;

    Matrix() {
        strings = 0;
        columns = 0;
        matrix = new double[strings][columns];
    }

    Matrix(double[][] matrix) {
        strings = matrix.length;
        columns = matrix[0].length;
        this.matrix = matrix;
    }

    Matrix(int strings, int columns) {
        this.strings = strings;
        this.columns = columns;
        matrix = new double[strings][columns];
    }

    int getStrings() {
        return strings;
    }

    int getColumns() {
        return columns;
    }

    double getAt(int n, int m) {
        return matrix[n][m];
    }

    void setAt(int n, int m, double value) {
        matrix[n][m] = value;
    }

    @Override
    public String toString() {
        StringBuilder stringInput = new StringBuilder();
        StringBuilder stringOutput = new StringBuilder();
        for (double[] strings : matrix) {
            for (double a : strings) {
                stringInput.append(a + ",");
            }
            stringInput.replace(stringInput.length() - 1,stringInput.length(), "");
            stringOutput.append(stringInput.toString() + ";\n");
            stringInput = new StringBuilder("");
        }
        return stringOutput.toString();
    }
}