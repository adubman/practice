package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

public class UndeclaredException extends RuntimeException {
    private static final long serialVersionUID = -2452840957489628105L;

    public UndeclaredException() {
    }

    UndeclaredException(String message) {
        super(message);
    }
}
