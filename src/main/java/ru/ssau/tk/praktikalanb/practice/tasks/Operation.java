package ru.ssau.tk.praktikalanb.practice.tasks;

abstract class Operation {
    abstract double apply(double number);

    double applyTriple(double number){
        return apply(apply(apply(number)));
    }
}