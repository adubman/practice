package ru.ssau.tk.praktikalanb.practice.tasks;

/*
    Сначала на консоль выведется Ignat, так как копия ссылки на объект переопределена методом checkAnotherPerson()
    новым объектом person, при том только во время работы метода.Потом выводится Arkadiy, так как
    происходит обращение к исходному объекту.
 */
public class classRorAssignmentFourteen {
    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("Arkadiy");
        checkAnotherPerson(person);
        System.out.println(person.getFirstName());
    }

    private static void checkAnotherPerson(Person person) {
        person = new Person();
        person.setFirstName("Ignat");
        System.out.println(person.getFirstName());
    }
}
