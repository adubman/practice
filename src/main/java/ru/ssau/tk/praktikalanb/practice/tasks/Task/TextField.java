package ru.ssau.tk.praktikalanb.practice.tasks.Task;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TextField extends JFrame {
    private JTextField field;

    private TextField() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        field = new JTextField(15);
        JButton countUp = new JButton("count up");
        countUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(TextField.this,
                        " Cлово наоборот: " + new StringBuffer(field.getText()).reverse().toString());
            }
        });

        JPanel contents = new JPanel(new FlowLayout(FlowLayout.LEFT));
        contents.add(field);
        contents.add(countUp);
        setContentPane(contents);
        setSize(400, 130);
        setVisible(true);
    }

    public static void main(String[] args) {
        new TextField();
    }
}
