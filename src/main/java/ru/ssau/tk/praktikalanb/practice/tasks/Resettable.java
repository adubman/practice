package ru.ssau.tk.praktikalanb.practice.tasks;

public interface Resettable {
   void reset();
}
