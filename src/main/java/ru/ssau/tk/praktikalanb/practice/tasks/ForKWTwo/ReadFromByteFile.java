package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.*;

//////b.	Создать класс с main-методом,
// который будет считывать из байтового файла информацию о человеке – фамилию, имя,
// отчество – и заполнять ею поля объекта Person.


public class ReadFromByteFile {
    public static void main(String[] args) {
        try(DataInputStream stream= new DataInputStream(new FileInputStream("first.txt"))){
            System.out.println(stream.readUTF());
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}

