package ru.ssau.tk.praktikalanb.practice.tasks;

import java.lang.*;
import java.util.Collection;
import java.util.LinkedList;

class Array {
    static double[] getNewArray(int size) {
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = Math.random() * 11;
        }
        return array;
    }

    static double[] getOneArray(int count) {
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            if (i == 0 || i == count) {
                array[i] = 2;
            } else {
                array[i] = 1;
            }
        }
        return array;
    }

    static double[] getOddArray(int count) {
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            array[i] = i * 2 + 1;
        }
        return array;
    }

    static double[] getEvenArray(int count) {
        double[] array = new double[count];
        for (int i = count - 1; i >= 0; i--) {
            array[i] = i * 2 + 2;
        }
        return array;
    }

    static double[] getFibonacci(int args) {
        int count = 7;
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            if (i == 0) {
                array[i] = 0;
            } else if (i < 2) {
                array[i] = 1;
            } else {
                array[i] = array[i - 2] + array[i - 1];
            }
        }
        return array;
    }

    static double[] getArrayOfSquares(int count) {
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            array[i] = i * i;
        }
        return array;
    }

    static double[] getArrayNotDivisibleByThree(int count) {
        double[] array = new double[count];
        int k = 1;
        for (int i = 0; i < count; i++) {
            if (k % 3 == 0) {
                k++;
            }
            array[i] = k;
            k++;
        }
        return array;
    }

    static double[] getArrayArithmeticProgression(double firstElement, double difference, int count) {
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            array[i] = firstElement + i * difference;
        }
        return array;
    }

    static double[] getArrayGeometricProgression(double firstElement, double denominator, int count) {
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            array[i] = firstElement * Math.pow(denominator, i);
        }
        return array;
    }

    static double[] getQuadraticSolution(double a, double b, double c) {
        if (a != 0) {
            double[] array = new double[2];
            double D = Math.pow(b, 2) - 4 * a * c;
            array[0] = (-b + Math.pow(D, 0.5)) / (2 * a);
            array[1] = (-b - Math.pow(D, 0.5)) / (2 * a);
            return array;
        } else {
            double[] array = new double[1];
            array[0] = -c / b;
            return array;
        }
    }

    static int[] getArrayIsSymmetrical(int count) {
        if (count == 0) {
            return null;
        }
        int[] array = new int[count];
        for (int i = 0; i < Math.round((double) count / 2); i++) {
            array[i] = i + 1;
            array[-i + count - 1] = i + 1;
        }
        return array;
    }

    static boolean getCheckingForPresenceOfNumberInArray(double[] array, double x) {
        for (double v : array) {
            if (v == x) {
                return true;
            }
        }
        return false;
    }

    static int getNumberOfEvenNumbers(double[] array) {
        int count = 0;
        for (double v : array) {
            if (v % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    static boolean getCheckingForNull(Integer[] array) {
        for (Integer integer : array) {
            if (integer == null) {
                return true;
            }
        }
        return false;
    }

    static int getSumOfAllNumbersWithEvenIndices(Integer[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                sum = sum + array[i];
            }
        }
        return sum;
    }

    static boolean getTakesIntegersAndReturnsBoolean(Integer[] array) {
        int k = 0;
        int p = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % array[0] == 0) {
                k++;
            }
            if (i % array[array.length - 1] == 0) {
                p++;
            }
        }
        return k > p;
    }

    static void getReverseSign(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = -(array[i]);

        }

    }

    static boolean[] getTruthOrLie(int[] array) {
        boolean[] total = new boolean[array.length];
        for (int i = 0; i < array.length; i++) {
            total[i] = array[i] % 2 == 0;
        }
        return total;
    }

    static double getElementThatOccursMostOftenInArray(double[] array) {
        double countOne = 0;
        double result = array[0];

        for (double value : array) {
            int countTwo = 1;
            for (double v : array) {
                if (v == value) {
                    countTwo++;
                }

                if (countTwo > countOne) {
                    result = value;
                    countOne = countTwo;
                }
            }
        }
        return result;
    }

    static Integer getInputIsArrayOfIntegersAndOutputIsMaximumElement(Integer[] array) {
        if (array.length == 0) {
            return null;
        }
        Integer maxValue = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    static int indexOIfFirstElementEqualToInputNumber(double[] array, double number) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        return -1;
    }

    static void swapElements(double[] array) {
        double firstMax = Double.MIN_VALUE;
        double firstMin = Double.MAX_VALUE;
        int indexOfMax = 0;
        int indexOfMin = 0;
        for (int j = 0; j < array.length; j++) {
            if (array[j] > firstMax) {
                firstMax = array[j];
                indexOfMax = j;
            }
            if (array[j] < firstMin) {
                firstMin = array[j];
                indexOfMin = j;
            }
        }
        array[indexOfMax] = firstMin;
        array[indexOfMin] = firstMax;
    }

    static void bitNegation(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = ~array[i];
        }
    }

    static int[] arrayBitNegation(int[] array) {
        int[] arrayBit = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            arrayBit[i] = ~array[i];
        }
        return arrayBit;
    }

    static int[] sumOfPairwiseElements(int[] array) {
        int[] arraySum;
        if (array.length % 2 == 0) {
            arraySum = new int[array.length / 2];
            for (int i = 0; i < arraySum.length; i++) {
                arraySum[i] = array[2 * i] + array[2 * i + 1];
            }
        } else {
            arraySum = new int[array.length / 2 + 1];
            for (int i = 0; i < arraySum.length - 1; i++) {
                arraySum[i] = array[2 * i] + array[2 * i + 1];
            }
            arraySum[arraySum.length - 1] = array[array.length - 1];
        }
        return arraySum;
    }

    static int[][] createTwoDimensionalArrayOfNumbers(int num) {
        int[][] result = new int[num][];
        int x = 1;
        for (int i = 0; i < num; i++) {
            result[i] = new int[num - i];
            for (int j = 0; j < num - i; j++) {
                result[i][j] = x;
                x++;
            }
        }
        return result;
    }

    static int[] createArrayAndFillItWithNaturalNumbers(int count, int index) {
        int[] array = new int[count];
        for (int i = 0; i < array.length; i++) {
            if ((i + index) < count) {
                array[i + index] = i + 1;
            } else {
                array[i + index - count] = i + 1;
            }
        }
        return array;
    }

    static double[] allIntegerDivisors(int number) {
        double[] array = new double[0];
        int count = 0;
        for (int i = 1; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                double[] arrayNew = new double[array.length + 1];
                System.arraycopy(array, 0, arrayNew, 0, array.length);
                arrayNew[count] = i;
                array = arrayNew;
                count++;
            }
        }
        return array;
    }

    static int[] longToInt(long num) {
        return new int[]{(int) (num >>> 32), (int) num};
    }

    static long intToLong(int[] num) {
        return ((long) num[0] << 32) | ((long) num[1] & 4294967295L);
    }

    static void containedOrNotInNanArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            if (Double.isNaN(array[i])) {
                return;
            } else {
                java.util.Arrays.sort(array);
            }
        }
    }

    static void printAllStringValuesToConsole(String[] array) {
        for (String s : array) {
            System.out.print(s + "\n");
        }
    }

    static double productOfAllNumbers(double[] array) {
        double num = 1;
        for (double v : array) {
            if (Double.isNaN(v) || v == Double.POSITIVE_INFINITY || v == 0) {
                continue;
            }
            num *= v;
        }
        return num;
    }

    static Collection<String> toHexString(int[] array) {
        Collection<String> stringArray = new LinkedList<>();
        java.util.Arrays.stream(array).forEach(value -> stringArray.add(Integer.toHexString(value)));
        return stringArray;
    }

    /* подправила метод allIntegerDivisors и вот получился новый в помощь для нахождения
     положительных простых чисел, то есть для метода positivePrimes*/
    private static double[] allIntegerDivisorsTwo(int number) {
        double[] array = new double[0];
        int count = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                double[] arrayNew = new double[array.length + 1];
                System.arraycopy(array, 0, arrayNew, 0, array.length);
                arrayNew[count] = i;
                array = arrayNew;
                count++;
            }
        }
        return array;
    }

    static double[] positivePrimes(int number) {
        double[] array = new double[0];
        int count = 0;
        for (int i = 1; i <= number; i++) {
            if (Array.allIntegerDivisorsTwo(i).length == 2) {
                double[] arrayNew = new double[array.length + 1];
                System.arraycopy(array, 0, arrayNew, 0, array.length);
                arrayNew[count] = i;
                array = arrayNew;
                count++;
            }
        }
        return array;
    }
}






