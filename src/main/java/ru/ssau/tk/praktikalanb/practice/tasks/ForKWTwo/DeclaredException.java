package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

public class DeclaredException  extends Exception{
    private static final long serialVersionUID = -3181988541259277715L;

    public  DeclaredException() {
    }

    public  DeclaredException(String message) {
        super(message);
    }
}
