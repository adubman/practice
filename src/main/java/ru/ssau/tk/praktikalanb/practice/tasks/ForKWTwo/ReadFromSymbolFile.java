package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;


import java.io.*;
////d.	Создать класс с main-методом, который будет считывать из
// символьного файла информацию о человеке – фамилию, имя, отчество,
// записанные через пробел – и заполнять ею поля объекта Person.


public class ReadFromSymbolFile {
    public static void main(String[] args) {
        try(BufferedReader stream= new BufferedReader(new FileReader("first.txt"))){
            System.out.println(stream.readLine());
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}


