package ru.ssau.tk.praktikalanb.practice.tasks;

public class PointsOutPut {
    public static void main(String[] args) {
        Point pointOne = new Point(10, 6, 4);
        Point pointTwo = new Point(3, 5, 9);
        System.out.println("First point: " + pointOne.x + ' ' + pointOne.y + ' ' + pointOne.z);
        System.out.println("Second point: " + pointTwo.x + ' ' + pointTwo.y + ' ' + pointTwo.z);
        System.out.println("Sum: " + Points.sum(pointOne, pointTwo).x + ' ' + Points.sum(pointOne, pointTwo).y + ' ' + Points.sum(pointOne, pointTwo).z);
        System.out.println("Subtract: " + Points.subtract(pointOne, pointTwo).x + ' ' + Points.subtract(pointOne, pointTwo).y + ' ' + Points.subtract(pointOne, pointTwo).z);
        System.out.println("Multiply: " + Points.multiply(pointOne, pointTwo).x + ' ' + Points.multiply(pointOne, pointTwo).y + ' ' + Points.multiply(pointOne, pointTwo).z);
        System.out.println("Divide: " + Points.divide(pointOne, pointTwo).x + ' ' + Points.divide(pointOne, pointTwo).y + ' ' + Points.divide(pointOne, pointTwo).z);
        System.out.println("Enlarge: " + Points.enlarge(pointOne, 8.98).x + ' ' + Points.enlarge(pointOne, 8.98).y + ' ' + Points.enlarge(pointOne, 8.98).z);
        System.out.println("Opposite: " + Points.opposite(pointOne).x + ' ' + Points.opposite(pointOne).y + ' ' + Points.opposite(pointOne).z);
        System.out.println("Inverse: " + Points.inverse(pointOne).x + ' ' + Points.inverse(pointOne).y + ' ' + Points.inverse(pointOne).z);
        System.out.println("vector Product: " + Points.vectorProduct(pointOne, pointTwo).x + ' ' + Points.vectorProduct(pointOne, pointTwo).y + ' ' + Points.vectorProduct(pointOne, pointTwo).z);
        System.out.println("scalar Product: " + Points.scalarProduct(pointOne, pointTwo));
        System.out.println("Length vector 1: " + Points.length(pointOne));
        System.out.println("Length vector2: " + Points.length( pointTwo));
    }
}

