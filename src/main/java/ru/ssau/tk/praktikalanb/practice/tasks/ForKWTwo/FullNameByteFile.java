package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.*;


///a.	Создать класс с main-методом, который будет сохранять полное имя из объекта Person в текстовый байтовый файл.
public class FullNameByteFile {
    public static void main(String[] arg) {
        Person person = new Person("Ivanov", "Ivan","Ivanovich");
        try(DataOutputStream stream= new DataOutputStream(new FileOutputStream("first.txt"))){
            stream.writeUTF(person.FullNameWithSpace());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

