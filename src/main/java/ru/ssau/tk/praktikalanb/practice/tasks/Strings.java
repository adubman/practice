package ru.ssau.tk.praktikalanb.practice.tasks;

import java.nio.charset.Charset;
import java.util.Objects;

class Strings {
    static char[] characterOnSeparateLine(String line) {
        char[] symbols = new char[line.length()];
        for (int i = 0; i < line.length(); i++) {
            symbols[i] = line.charAt(i);
        }
        return symbols;
    }

    static void byteArray(String string) {
        byte[] bytes = string.getBytes();
        for (byte byteArray : bytes) {
            System.out.println(byteArray);
        }
    }

    static void comparisonsOfTwoLines() {
        String stringOne = "Samara University";
        String stringTwo = new String(stringOne);
        System.out.println(stringOne == stringTwo);
        System.out.println(stringOne.equals(stringTwo));
    }

    static boolean stringIsPalindrome(String line) {
        for (int i = 0; i < line.length() / 2; ++i) {
            if (line.charAt(i) != line.charAt(line.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    static boolean stringsAreDifferent(String lineOne, String lineTwo) {
        if (!Objects.equals(null, lineTwo) && !Objects.equals(lineOne, null) && !lineOne.equals(lineTwo) && lineOne.equalsIgnoreCase(lineTwo)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int i = 0;
        System.out.println("Символ\t№" + ++i); //  табуляция
        System.out.println("Символ\b№" + ++i); //  удаление одного символа в строке
        System.out.println("Символ\n№" + ++i); // переход на новую строку
        System.out.println("Символ\r№" + ++i); // возврат каретки
        System.out.println("Символ\'№" + ++i);  // символ одинарной кавычки
        System.out.println("Символ\"№" + ++i);  // символ двойной кавычки
        System.out.println("Символ\\№" + ++i); // символ обратной косой черты
        //Если в последнем случае убрать один символ \, то будет ошибка Illegal escape character in string literal
    }

    static int returnIndexCharacterOfFirstLine(String lineOne, String lineTwo) {
        return lineOne.indexOf(lineTwo);
    }

    static int indexOfFirstOccurrenceOfSecondRowInSecondHalfOfFirstRow(String lineOne, String lineTwo) {
        return lineOne.indexOf(lineTwo, lineOne.length() / 2);
    }

    static int indexOfLastOccurrenceOfSecondRowInFirstHalfOfFirstRow(String lineOne, String lineTwo) {
        return lineOne.lastIndexOf(lineTwo, lineOne.length() / 2);
    }

    static String replacementInFirstLineEachOccurrenceOfSecondLineOnThirdLine(String lineOne, String lineTwo, String lineThree) {
        for (int i = 0; i < 100; i++)
            if (lineOne.contains(lineTwo)) {
                lineOne = lineOne.replaceAll(lineTwo, lineThree);
            } else {
                break;
            }
        return lineOne;
    }

    static int methodReturningNumberOfRows(String[] lines, String prefixLine, String suffixLine) {
        int numberOfRows = 0;
        for (String line : lines) {
            if (line.startsWith(prefixLine) && line.endsWith(suffixLine)) numberOfRows++;
        }
        return numberOfRows;
    }

    static String stringAndTwoIntegers(String string, int from, int to) {
        if (from >= to)
            return "";
        if (from < 0)
            from = 0;
        if (to > string.length())
            to = string.length();
        return string.substring(from, to);
    }

    static int numberOfLinesFromPrefixToPostfixIgnoringSpaces(String[] lines, String prefix, String suffix) {
        int j = 0;
        for (String line : lines) {
            if (line.trim().startsWith(prefix) && line.trim().endsWith(suffix)) {
                j++;
            }
        }
        return j;
    }

    static String replaceEachEvenCharacterWithNumberOfThatCharacterAndFlipLine(String line) {
        StringBuilder builder = new StringBuilder(line);
        for (int i = 0; i < line.length(); i++) {
            if (i % 2 == 0) {
                builder.replace(i, i + 1, Integer.toString(i));
            }
        }
        return builder.reverse().toString();
    }

    static String translationOfInputStringFromFirstEncodingToSecond(String string, Charset oneCharset, Charset twoCharset) {
        byte[] bytes = string.getBytes(oneCharset);
        return new String(bytes, twoCharset);
    }

    static void printInConsole(Object obj) {
        System.out.println("Описание объекта: " + "\n" + obj);
    }

    static String[] firstCharacterMustBeUppercase(String string) {
        String[] newString = string.split(" ");
        for (int i = 0; i < newString.length; i++) {
            if (!newString[i].isEmpty()) {
                newString[i] = newString[i].substring(0, 1).toUpperCase() + newString[i].substring(1);
            }
        }
        return newString;
    }

    static void defaultEncodings() {
        System.out.println(java.nio.charset.Charset.defaultCharset());
    }

    static String combineInOneLine(String[] string) {
        return String.join(", ", string);
    }

    static String formStringConsistingOfEnumeration(int num) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(0);
        for (int i = 1; i < num; i++) {
            stringBuilder.append(" ").append(i);
        }
        return stringBuilder.toString();
    }

}


