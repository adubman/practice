package ru.ssau.tk.praktikalanb.practice.tasks;

public interface IntGenerator {
    int nextInt();
}