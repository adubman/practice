package ru.ssau.tk.praktikalanb.practice.tasks;

class Point {
    final public double x, y, z;

    Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double length() {
        return  Math.sqrt(x * x + y * y + z * z);
    }

    @Override
    public String toString() {
        return "[".concat(String.valueOf(x)).concat(", ").concat(String.valueOf(y)).concat(", ").concat(String.valueOf(z).concat("]"));
    }
}

