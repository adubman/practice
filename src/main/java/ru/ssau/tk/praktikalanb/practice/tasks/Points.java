package ru.ssau.tk.praktikalanb.practice.tasks;

class Points {

    private static double precision = 0.00005;
    private Points() {
    }

    static Point sum(Point pointsOne, Point pointsTwo) {
        return new Point(pointsOne.x + pointsTwo.x, pointsOne.y + pointsTwo.y, pointsOne.z + pointsTwo.z);
    }

    static Point subtract(Point pointsOne, Point pointsTwo) {
        return new Point(pointsOne.x - pointsTwo.x, pointsOne.y - pointsTwo.y, pointsOne.z - pointsTwo.z);
    }

    static Point multiply(Point pointsOne, Point pointsTwo) {
        return new Point(pointsOne.x * pointsTwo.x, pointsOne.y * pointsTwo.y, pointsOne.z * pointsTwo.z);
    }

    static Point divide(Point pointsOne, Point pointsTwo) {
        return new Point(pointsOne.x / pointsTwo.x, pointsOne.y / pointsTwo.y, pointsOne.z / pointsTwo.z);
    }

    static Point enlarge(Point points, double number) {
        return new Point(points.x * number, points.y * number, points.z * number);
    }

    static Point opposite(Point points) {
        return new Point(-points.x, -points.y, -points.z);
    }

    static Point inverse(Point points) {
        return new Point(1 / points.x, 1 / points.y, 1 / points.z);
    }

    static double scalarProduct(Point pointsOne, Point pointsTwo) {
        return pointsOne.x * pointsTwo.x + pointsOne.y * pointsTwo.y + pointsOne.z * pointsTwo.z;
    }

    static Point vectorProduct(Point pointsOne, Point pointsTwo) {
        return new Point(pointsOne.y * pointsTwo.z - pointsOne.z * pointsTwo.y, pointsOne.z * pointsTwo.x - pointsOne.x * pointsTwo.z, pointsOne.x * pointsTwo.y - pointsOne.y * pointsTwo.x);
    }

    public static double length(Point points) {
        return points.length();
    }

    private static boolean equalsApproximately(double one, double two) {
        return Math.abs(one - two) < precision;
    }

    static boolean equalsApproximately(Point one, Point two) {
        return equalsApproximately(one.x, two.x) && equalsApproximately(one.y, two.y) && equalsApproximately(one.z, two.z);
    }

}
