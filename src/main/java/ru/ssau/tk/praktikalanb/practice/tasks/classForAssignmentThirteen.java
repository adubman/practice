package ru.ssau.tk.praktikalanb.practice.tasks;

/*На консоль сначала выведется 10, это происходит из-за того, что переменная number переопределена,
так как вызывается метод checkInt() , внутри которого этот метод меняет значение переменной на 10 и выводит его. Потом выводится Oleg,
так как передаётся копия ссылки на него, которая будет указывать на тот же объект. Потом выводится 5,
так как метод checkInt() изменил не саму переменную number, а её копию, определенную только в методе из этого следует,
что выводится значение переменной которая задается изначально. Затем снова выводится Oleg,
так как ссылка указывает на тот же объект, на который указывала копия ссылки.
 */
public class classForAssignmentThirteen {
    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("Arkadiy");
        int number = 5;
        checkInt(number);
        checkPerson(person);
        System.out.println(number);
        System.out.println(person.getFirstName());
    }

    private static void checkInt(int number) {
        number = 10;
        System.out.println(number);
    }

    private static void checkPerson(Person person) {
        person.setFirstName("Oleg");
        System.out.println(person.getFirstName());
    }
}
