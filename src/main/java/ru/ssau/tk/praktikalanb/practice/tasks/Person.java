package ru.ssau.tk.praktikalanb.practice.tasks;

class Person {
    private String firstName;
    private String lastName;
    private int passportId;
    private Gender gender;

    public Person() {
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String gender, String firstName, String lastName, int passportId ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passportId = passportId;
        this.gender = Gender.valueOf(gender);
    }


    public Person(int passportId) {
        this.passportId = passportId;
    }

    String getFirstName() {
        return firstName;
    }

    String getLastName() {
        return lastName;
    }

    int getPassportId() {
        return passportId;
    }

    Gender getGender() {
        return gender;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    void setPassportId(int passportId) {
        this.passportId = passportId;
    }

   void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return firstName.concat(" ").concat(lastName);
    }



}


