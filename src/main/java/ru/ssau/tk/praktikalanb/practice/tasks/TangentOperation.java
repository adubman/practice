package ru.ssau.tk.praktikalanb.practice.tasks;

class TangentOperation extends Operation {
    @Override
    double apply(double number) {
        return Math.tan(number);
    }
}
