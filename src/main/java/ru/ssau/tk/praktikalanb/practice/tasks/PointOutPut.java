package ru.ssau.tk.praktikalanb.practice.tasks;

public class PointOutPut {
    public static void main(String[] args) {
        Point pointOne = new Point(3, 1, 2);
        Point pointTwo= new Point(3.6, -0.9, 0.1);
        System.out.println(pointOne);
        System.out.println(pointTwo);
    }
}
