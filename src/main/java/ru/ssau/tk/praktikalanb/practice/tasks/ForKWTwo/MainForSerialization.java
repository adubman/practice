package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


////////e.	Подготовить класс Person к сериализации.
// Создать класс с main-методом, который будет сериализовывать
// три разных объекта класса Person в один файл.
public class MainForSerialization {
    public static void main(String[] args) throws IOException {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person.txt"))) {
            Person p = new Person("Socolov", "Kola", "Ivanovich");
            oos.writeObject(p);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}

