package ru.ssau.tk.praktikalanb.practice.tasks;

class LearningWrappers {

    @Deprecated
    static Integer boxing(int i) {
        return new Integer(i);
    }

    @Deprecated
    static Boolean boxing(boolean b) {
        return new Boolean(b);
    }

    @Deprecated
    static Short boxing (short s) {
        return new Short(s);
    }

    @Deprecated
    static Double boxing (double d) {
        return new Double(d);
    }

    @Deprecated
    static Float boxing (float f) {
        return new Float(f);
    }

    @Deprecated
    static Long boxing (long l){
        return new Long(l);
    }

    @Deprecated
    static Character boxing (char c) {
        return new Character(c);
    }

    @Deprecated
    static Byte boxing (byte byt) {
        return new Byte(byt);
    }

    static int unboxing(Integer i) {
        return i.intValue();
    }

    static boolean unboxing(Boolean b) {
        return b.booleanValue();
    }

    static short unboxing(Short s) {
        return s.shortValue();
    }

    static double unboxing(Double d) {
        return d.doubleValue();
    }

    static float unboxing(Float f) {
        return f.floatValue();
    }

    static long unboxing(Long l) {
        return l.longValue();
    }

    static char unboxing(Character c) {
        return c.charValue();
    }

    static byte unboxing(Byte byt){
        return byt.byteValue();
    }

    static Integer autoBoxing(int p) {
        return p;
    }

    static Boolean autoBoxing(boolean b) {
        return b;
    }

    static Short autoBoxing(short sh) {
        return sh;
    }

    static Double autoBoxing(double d) {
        return d;
    }

    static Float autoBoxing(float f) {
        return f;
    }

    static Long autoBoxing(long l) {
        return l;
    }

    static Character autoBoxing(char c) {
        return c;
    }

    static Byte autoBoxing(byte b) {
        return b;
    }

    static int autoUnboxing(Integer i) {
        return i;
    }

    static boolean autoUnboxing(Boolean b) {
        return b;
    }

    static short autoUnboxing(Short s) {
        return s;
    }

    static double autoUnboxing(Double d) {
        return d;
    }

    static float autoUnboxing(Float f) {
        return f;
    }

    static long autoUnboxing(Long l) {
        return l;
    }

    static char autoUnboxing(Character c) {
        return c;
    }

    static byte autoUnboxing(Byte byt){
        return byt;
    }
}