package ru.ssau.tk.praktikalanb.practice.tasks.ForKWTwo;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
////////f.	Подготовить класс Person к сериализации.
// Создать класс с main-методом, который будет десериализовывать
// три разных объекта класса Person из файла.
public class MainForDeserialization {
    public static void main(String[] args) {

        try(ObjectInputStream stream = new ObjectInputStream(new FileInputStream("person.txt")))
        {
            Person p=(Person)stream.readObject();
            System.out.printf("FName: %s \t Name: %s \t Patronymic: %s \t", p.getFName(), p.getName(),  p.getPatronymic() );
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
