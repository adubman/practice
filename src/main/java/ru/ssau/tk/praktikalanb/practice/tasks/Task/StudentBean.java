package ru.ssau.tk.praktikalanb.practice.tasks.Task;

import java.util.*;

public class StudentBean {
    private String Name;
    private String Surname;
    private String MiddleName;
    private int Number;
    private Map<Integer, int[]> grades = new LinkedHashMap<>();

    private StudentBean(String Name, String Surname, String MiddleName, int Number) {
        this.Name = Name;
        this.Surname = Surname;
        this.MiddleName = MiddleName;
        this.Number = Number;
    }

    public StudentBean() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int Number) {
        this.Number = Number;
    }


    public void setGrades(int Number, int... grades) {
        this.grades.put(Number, grades);
    }

    public int[] getGrades(int Number) {
        return this.grades.get(Number);
    }

    public static void main(String[] args) {
        StudentBean student = new StudentBean("Artur", "Ivanow", "Ivanowic", 2);
        Map<Integer, int[]> grades = new LinkedHashMap<>();
        grades.put(2, new int[]{4, 4, 4, 5});
        grades.put(3, new int[]{4, 5, 4, 5});
        grades.put(4, new int[]{4, 5, 5, 5});

        String exists = (grades.containsKey(2)) ? "найден" : "не найден";
        System.out.println("Объект с ключом '2' " + exists);
        for (Integer one : grades.keySet()) {
            System.out.print("Студент " + student.getSurname() + " " + student.getName() + " " + student.getMiddleName() + " " + "Номер сессии : " + one + " Оценки: " + Arrays.toString(grades.get(one)) + "\n");
        }


        StudentBean studentTwo = new StudentBean();
        studentTwo.setSurname("Petrow");
        studentTwo.setName("Petr");
        studentTwo.setMiddleName("Ivanovich");
        Map<Integer, int[]> gradesTwo = new LinkedHashMap<>();
        gradesTwo.put(1, new int[]{4, 4, 4, 5});
        gradesTwo.put(2, new int[]{4, 5, 4, 5});
        gradesTwo.put(3, new int[]{4, 5, 5, 5});

        for (Integer two : gradesTwo.keySet()) {
            System.out.print("Студент " + studentTwo.getSurname() + " " + studentTwo.getName() + " " + studentTwo.getMiddleName() + " " + "Номер сессии : " + two + " Оценки: " + Arrays.toString(gradesTwo.get(two)) + "\n");
        }
    }
}
