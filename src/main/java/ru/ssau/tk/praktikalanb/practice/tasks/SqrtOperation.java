package ru.ssau.tk.praktikalanb.practice.tasks;

class SqrtOperation extends Operation{
    @Override
    double apply(double number) {
        return Math.sqrt(number);
    }
}

