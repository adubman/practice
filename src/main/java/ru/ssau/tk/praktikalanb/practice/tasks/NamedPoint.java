package ru.ssau.tk.praktikalanb.practice.tasks;

class NamedPoint extends Point implements Resettable {
    private String name;

    NamedPoint(double x, double y, double z) {
        super(x, y, z);
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    NamedPoint(double x, double y, double z, String name) {
        super(x, y, z);
        this.name = name;
    }

    NamedPoint() {
        super(0,0,0);
        this.name = "Origin";
    }

    NamedPoint(String name,double x, double y, double z ) {
        super(x, y, z);
        this.name = name;
    }

    @Override
    public String toString() {
        if (this.name == null) {
            return super.toString();
        } else {
            return "" + this.name + " " + super.toString();
        }
    }

    @Override
    public void reset() {
        name = "Absent";
    }
}
